```
 _   _   ____  _______  ______  __  __ 
| \ | | / __ \|__   __||  ____||  \/  |
|  \| || |  | |  | |   | |__   | \  / |
| . ` || |  | |  | |   |  __|  | |\/| |
| |\  || |__| |  | |   | |____ | |  | |
|_| \_| \____/   |_|   |______||_|  |_|
```
Created by Ben Doty (Twitter: @beendoty)

Copyright 2020

## What is it?

Notem is a markdown note and notebook manager

Notem was created to allow you to create notes wtih metadata quickly and easily.

Each note has auto-generated metadata included at the top of the file. The metadata can be changed manually, if required.


## How does it work?

Before a note can be created, there must be a place to store the notes.
 
Notem searches for the note directory on the local machine. If it does not exist you wil be prompted to create one. You can create your own, or use the default. A git repository can also be used.

Once the directory has been confirmed, you will be able to use the program normally.

### For notes

1. Upon reciving command line arguments specific to the note, Notem creates a file in the notes directory. 
2. Notem writes metadata at the top of the file.
3. The file is closed.
4. The file is opened in the default text editor or the editor of your choice.

### For notebooks

1. Upon reciving command line arguments specific to the notebook, Notem creates a folder within the notes directory.
2. The folder is closed and can now be used as a notebook to group notes.


